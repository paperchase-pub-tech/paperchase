// Articles
// --------
articles.before.insert(function (userId, doc) {
    // console.log('..articles before insert');
    var updatedBy = {};
    // Volume, Issue
    if(doc.volume && doc.issue){
        volume = doc.volume;
        issue = doc.issue;
        doc.issue_id = Meteor.call('articleIssueVolume',volume,issue);
    }
    // console.log(doc.issue_id);
    // track updates
    if(!doc.doc_updates){
        doc.doc_updates = {};
        doc.doc_updates.updates = [];
    }
    else if(doc.doc_updates && !doc.doc_updates.updates){
        doc.doc_updates.updates = [];
    }

    if(userId){
        updatedBy.user = userId;
    }

    updatedBy.date = new Date();
    doc.doc_updates.updates.push(updatedBy);
});
articles.after.insert(function (userId, doc) {
    var journal = journalConfig.findOne();
    // Pages - Keep issue doc pages up to date
    if( doc.page_start || doc.page_end ){
        if( doc.issue_id ){
            Meteor.call('updateIssuePages', doc.issue_id, function(error, result){
                if(error){
                    console.error('updateIssuePages', error);
                }
            });
        }
    }

    // Advance. Update sorters collection.
    if(journal && journal.journal && journal.journal.short_name && journal.journal.short_name != 'oncotarget'){
        if(doc.advance){
            Meteor.call('sorterAddItem', 'advance', doc._id);
        }
    }
});
articles.before.update(function (userId, doc, fieldNames, modifier, options) {
    var volume,
        issue,
        previous;

    // for when we want to skip things in the hook
    if(modifier.$set && modifier.$set.batch){
        delete modifier.$set.batch;
    }

    // maintain Files
    if(modifier.$set && modifier.$set.files){
        for(var fileType in doc.files){
            if(!modifier.$set.files[fileType]){
                modifier.$set.files[fileType] = doc.files[fileType];
            }
        }
    }

    if(modifier.$set){
        previous = articles.findOne({_id : doc._id});
        delete previous.previous;
        modifier.$set.previous = previous;
    }

    // Affiliations
    //add affiliation number to author
    //might need to adjust this as article updates get added
    if(fieldNames.indexOf('authors') != -1){
        var authorsList = modifier.$set.authors;
        var affiliationsList = doc.affiliations;
        for(var i = 0 ; i < authorsList.length ; i++){
            if(authorsList[i].affiliations_names && affiliationsList){
                //article update from a batch import of author affiliations
                //affiliations_names is only used to find index of affiliation after batch import
                authorsList[i].affiliations_numbers = [];
                for(var a = 0 ; a < authorsList[i].affiliations_names.length ; a++){
                    var affiliationIndex = affiliationsList.indexOf(authorsList[i].affiliations_names[a]);
                    authorsList[i].affiliations_numbers.push(parseInt(affiliationIndex));
                }
            }
            else if(authorsList[i].affiliations_numbers){

            }
        }
    }

    if(modifier.$set && modifier.$set.issue_id && !modifier.$set.volume){
        issueData = issues.findOne({_id : modifier.$set.issue_id});
        if(issueData && issueData.volume && issueData.issue){
            modifier.$set.volume = issueData.volume;
            modifier.$set.issue = issueData.issue;
        }
    } else if(modifier.$set && modifier.$set.volume && modifier.$set.issue){
        volume = modifier.$set.volume;
        issue = modifier.$set.issue;
        modifier.$set.issue_id = Meteor.call('articleIssueVolume',volume,issue);
    }

    // authors
    if(modifier.$set && modifier.$set.authors && modifier.$set.authors.length === 0){
        console.log('MISSING AUTHORS: ' + doc._id);
    }

    if(modifier.$set){
        modifier.$set.doc_updates = Meteor.db.trackUpdates(userId, doc, fieldNames, modifier, options);

        if( modifier.$set.ojsUser){
            // we use this in trackUpdates
            delete modifier.$set.ojsUser;
        }

        //recording this for easy sorting
        modifier.$set.last_update = new Date();
    }
});

articles.after.update(function (userId, doc, fieldNames, modifier, options) {
    var journal = journalConfig.findOne();
    // Advance article. Update sorters collection.
    if(journal.journal.short_name != 'oncotarget'){
        if(doc.advance && !this.previous.advance){
            Meteor.call('sorterAddItem', 'advance', doc._id);
        } else if(!doc.advance  && this.previous.advance){
            Meteor.call('sorterRemoveItem', 'advance', doc._id);
        }
    }

    // Pages - Keep issue doc pages up to date
    if( doc.page_start || doc.page_end || this.previous.page_start || this.previous.page_end ){
        if( doc.page_start != this.previous.page_start || doc.page_end != this.previous.page_end || doc.issue_id != this.previous.issue_id || !doc.display && this.previous.display || doc.display && !this.previous.display ){
            // Update issue
            if( doc.issue_id ){
                Meteor.call('updateIssuePages', doc.issue_id, function(error, result){
                    if(error){
                        console.error('updateIssuePages', error);
                    }
                });
            }
            if( this.previous.issue_id && doc.issue_id && doc.issue_id != this.previous.issue_id ){
                // Article changed issues, update both issues page spans
                Meteor.call('updateIssuePages', this.previous.issue_id, function(error, result){
                    if(error){
                        console.error('updateIssuePages', error);
                    }
                });
            }
        }
    }

    Meteor.call('recacheArticleHtml', doc._id);
});

// Article Types
// -----
articleTypes.before.insert(function (userId, doc) {
    // Track updates by user/date
    var createdBy = {};
    doc.doc_updates = {};
    doc.doc_updates.updates = [];

    if(userId){
        createdBy.user = userId;
    }

    createdBy.date = new Date();
    createdBy.created = true;
    doc.doc_updates.updates.push(createdBy);
});
articleTypes.before.update(function (userId, doc, fieldNames, modifier, options) {
    // Track updates by user/date
    if(modifier.$set){
        modifier.$set.doc_updates = Meteor.db.trackUpdates(userId, doc, fieldNames, modifier, options);
        modifier.$set.last_update = new Date();
    }

    // keep previous record
    if(modifier.$set){
        previous = articleTypes.findOne({_id : doc._id});
        delete previous.previous;
        modifier.$set.previous = previous;
    }
});

// Issues
// -----
issues.after.insert(function (userId, doc) {
    // console.log('..before insert issues');
    var issueData = {};
    issueData.doc_updates = {};
    issueData.doc_updates.created_date = new Date();
    issueData.doc_updates.created_by = userId;
});
issues.before.update(function (userId, doc, fieldNames, modifier, options) {
    // Current issue
    //------------
    if(modifier.$set && modifier.$set.current){
        // Update previously current issue, set as not current
        var previouslyCurrent = issues.findOne({current: true});
        if(previouslyCurrent && previouslyCurrent._id != doc._id){
            var prevMod = {current:false};
            var prevNext = Meteor.issue.prevNextDisplay(previouslyCurrent.volume, previouslyCurrent.issue, previouslyCurrent._id);
            if (prevNext && prevNext.prev || prevNext.next) {
                prevMod.prev_next = prevNext;
            }

            Meteor.call('updateIssue',previouslyCurrent._id, prevMod, function(error,result){
                    if(error){
                        console.error('changing current issue', error);
                    }
                    else {
                        Meteor.call('publishIssue', previouslyCurrent._id, function(err, res) {
                                if(err){
                                    console.error('Publishing formerly current issue', err);
                                }
                            });
                    }
                });
        }

        // remove all articles in the issue from advance
        var articlesInIssue = articles.find({ issue_id: doc._id }).fetch();
        articlesInIssue.forEach(function(article){
            Meteor.call('updateArticle', article._id, {advance: false, aop: false}, function(error,result){
                if(error){
                    console.error('removing article from advance failed', aritcle._id, error);
                }
            });
        });
    }

    // update prev/next, but only if needed
    var recalcPrevNext = true;
    if (modifier && modifier.$set && modifier.$set.volume && modifier.$set.issue){
        if (doc && doc.volume && doc.issue && modifier.$set.volume === doc.volume && modifier.$set.issue === doc.issue && doc.prev_next) {
            recalcPrevNext = false;
        }
        if (recalcPrevNext && modifier.$set.volume && modifier.$set.issue) {
            var prevNext = Meteor.issue.prevNextDisplay(modifier.$set.volume, modifier.$set.issue, doc._id);
            if (prevNext && prevNext.prev || prevNext.next) {
                modifier.$set.prev_next = prevNext;
            }
        }
    }

    // track updates
    // -------------
    modifier.$set.doc_updates = Meteor.db.trackUpdates(userId, doc, fieldNames, modifier, options);
    if(modifier.$set.ojsUser){
        // we use this in trackUpdates
        delete modifier.$set.ojsUser;
    }
});
issues.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('recacheIssueHtml', doc._id);
});


// News
// -----
newsList.after.insert(function (userId, doc) {
    var updateObj = {};
    updateObj.doc_updates = {};
    updateObj.doc_updates.created = {};
    updateObj.doc_updates.created.date = new Date();
    updateObj.doc_updates.created.user = userId;
    newsList.update({_id: doc._id},{$set:updateObj});
});

newsList.after.update(function (userId, doc, fieldNames, modifier, options) {
    // console.log('..after update news');console.log(modifier);
    var updateObj = {};
    updateObj.date = new Date();
    updateObj.user = userId;
    newsList.direct.update({_id : doc._id}, {$addToSet : {'doc_updates.updates' : updateObj}}); // MUST use direct.update, otherwise will be stuck in an update loop
    Meteor.call('recacheNewsHtml');
});

// Papers Sections
// ----------------
sections.before.insert(function(userId, doc) {
    var updatedBy = {};
    // track updates
    if(!doc.doc_updates){
        doc.doc_updates = {};
        doc.doc_updates.updates = [];
    } else if(doc.doc_updates && !doc.doc_updates.updates){
        doc.doc_updates.updates = [];
    }

    if(userId){
        updatedBy.user = userId;
    }

    updatedBy.date = new Date();
    doc.doc_updates.updates.push(updatedBy);
});
sections.after.insert(function (userId, doc) {
    // append to sorters collection if displaying
    if(doc.display){
        Meteor.call('sorterAddItem','sections',doc._id);
    }
});
sections.before.update(function (userId, doc, fieldNames, modifier, options) {
    if(modifier.$set){
        modifier.$set.doc_updates = Meteor.db.trackUpdates(userId, doc, fieldNames, modifier, options);
    }
});
sections.after.update(function (userId, doc, fieldNames, modifier, options){
    if (Meteor.settings.public && Meteor.settings.public.journal && Meteor.settings.public.journal.name && Meteor.settings.public.journal.name != 'Oncotarget'){
        if(modifier.$set && modifier.$set.display) {
            Meteor.call('sorterAddItem','sections',doc._id);
        } else{
            Meteor.call('sorterRemoveItem','sections',doc._id);
        }
    }

    Meteor.call('recacheSectionHtml', doc._id);
});

// Sorters
// -------
sorters.before.update(function (userId, doc, fieldNames, modifier, options) {
    if (Meteor.settings.public && Meteor.settings.public.journal && Meteor.settings.public.journal.name && Meteor.settings.public.journal.name === 'Oncotarget'){
        var advanceList = sorters.findOne({ name : 'advance' }).order;
        if (modifier && modifier.$push && modifier.$push.order && modifier.$push.order.$each){
            var toCheck = [];
            var verifiedOk = [];
            if (typeof modifier.$push.order.$each === 'string'){
                toCheck.push(modifier.$push.order.$each);
            } else {
                toCheck = modifier.$push.order.$each;
            }

            toCheck.forEach(function(mongoId){
                if(advanceList.indexOf(mongoId) === -1){
                    verifiedOk.push(mongoId);
                }
            });
            modifier.$push.order.$each = verifiedOk;
        }
    }
});
sorters.after.update(function (userId, doc, fieldNames, modifier, options){
    // if(modifier.$pull !== undefined) {
        // var article_id = modifier.$pull;
        // article_id = article_id.order;
        // if(article_id){
        //     articles.direct.update({_id:article_id}, {$set: {advance:false}});
        // }
    // }
    var advanceArray, duplicates;

    if(Meteor.settings.public && Meteor.settings.public.journal && Meteor.settings.public.journal.name && Meteor.settings.public.journal.name === 'Oncotarget'){
        advanceArray = sorters.findOne({ name : 'advance' }).order;
        if (advanceArray) {
            duplicates = Meteor.organize.arrayDuplicates(advanceArray);

            // duplicate found, notify via emails
            var message = 'Duplicate Mongo ID found for ' + Meteor.settings.public.journal.name + ' advance \n' + duplicates.toString();

            if(duplicates.length > 0){
                Email.send({
                   to: Meteor.settings.it.email,
                   from: Meteor.settings.it.email,
                   subject: 'Duplicate Advance Mongo ID',
                   text: message
                });
            }
        }
    }

    if (doc && doc.name && doc.name === 'advance') {
        Meteor.call('recacheAdvanceHtml');
    } else if(doc && doc.name && doc.name === 'forAuthors') {
        Meteor.call('recacheForAuthorsHtml');
    } else if(doc && doc.name && doc.name === 'about') {
        Meteor.call('recacheAboutHtml');
    } else if(doc && doc.name && doc.name === 'ethics') {
        Meteor.call('recacheEthicsHtml');
    }
});

// Editorial Board
// ---------
edboard.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('recacheEdboardHtml');
});

// For Authors
// ------------
forAuthors.after.insert(function (userId, doc) {
    Meteor.call('sorterAddItem', 'forAuthors', doc._id);
});
forAuthors.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('recacheForAuthorsHtml');
});

// About
// ------------
about.after.insert(function (userId, doc) {
    Meteor.call('sorterAddItem', 'about', doc._id);
});
about.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('recacheAboutHtml');
});

// Ethics
// ------------
ethics.after.insert(function (userId, doc) {
    Meteor.call('sorterAddItem', 'ethics', doc._id);
});
ethics.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('recacheEthicsHtml');
});

// Home Page
// ------------
homePage.after.insert(function (userId, doc) {
    Meteor.call('sorterAddItem', 'homePage', doc._id);
});
homePage.after.update(function (userId, doc, fieldNames, modifier, options) {
    Meteor.call('recacheHomePageHtml');
});
