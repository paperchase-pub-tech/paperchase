Npm.require = function(moduleName) {
  var module = Npm.require(moduleName);
  return module;
};

Meteor.require = function(moduleName) {
  console.warn('Meteor.require is deprecated. Please use Npm.require instead!');
  return Npm.require(moduleName);
};