// PubMed UI
// ----------------
Template.AdminPubMed.events({
    'submit .pubMedReportForm': function(e){
        e.preventDefault();
    },
    'click .generateReport ': function(e){
        var config = journalConfig.findOne();
        var piis = $('.piiList').val().replace(/\s|"|'/g, '').split(',');
        var reportName = $('.reportName').val();
        Session.set('pubMedReportName', reportName);
        Session.set('pubMedPiis', piis);
        Meteor.call('clearPubMedReport', reportName);
        piis.forEach(function(el,ind,arr) {
                var pii = el;
                pubMedReport.insert({reportName:reportName, pii:pii, "fresh":false,status:"initializing_report"});
            }); 
        Meteor.call('createPubMedReport', {piis:piis, reportName:reportName, resume:true});
    },
    'keyup .reportName, keyup .piiList' : function(e,t) {
        if(t.find('.reportName').value != '' && t.find('.piiList').value != '') {
            $(".generateReport").removeClass('disabled'); 
        }
        else {
            $(".generateReport").addClass('disabled'); 
        }
    }
});

