// Article Type Events
// ----------------
Template.ArticleTypeForm.events({
    'submit form': function(e,t){
        Meteor.articleTypes.submitForm(e,t);
    }
});
