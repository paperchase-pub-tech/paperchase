// Sections
// ----------------
Template.AdminSectionsForm.events({
    'submit form': function(e){
        Meteor.formActions.saving();
        Meteor.adminSections.formGetData(e);
    }
});
