Template.AdminArchive.events({
    'click #all-issue-publish': function() {
        Meteor.call('publishAllIssues', function(error,result){
            if(error){
                console.error('publishAllIssues',error);
                Meteor.formActions.errorMessage('Could not publish the issue.<br>' + error.error);
            }
        });
    }
});

