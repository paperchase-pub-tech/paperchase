Template.PageSectionForm.events({
    'submit form': function(e){
        Meteor.formActions.saving();
        Meteor.pageWithSections.submitFormData(e);
    }
});
Template.PageWithSections.events({
    'click #add-section': function(e){
        Session.set('showForm', true);
        Session.set('sectionId', null);

        $('html, body').animate({
            scrollTop: $('#add-section-container').position().top
        }, 500);
    },
    'click .edit-section': function(e){
        e.preventDefault();
        var sectionId = $(e.target).attr('id');
        Session.set('sectionId',sectionId);
    },
    'click #save-section-order': function(e){
        e.preventDefault();
        Meteor.formActions.saving();
        var order = [];
        $('.sections-list li').each(function(){
            var sectionMongoId = $(this).attr('id').replace('section-title-','');
            order.push(sectionMongoId);
        });

        var page = Meteor.pageWithSections.pageNameFromDataAttr(e);

        if (page) {
            Meteor.call('updateList', page, order, function(error,result){
                if(error){
                    console.log('error - updateList', error);
                    Meteor.formActions.errorMessage('Cannot update order. Please contact IT.');
                } else if(result){
                    Meteor.pageWithSections.successPageSection();
                }
            });
        } else {
            Meteor.formActions.errorMessage('Cannot determine page. Please contact IT.');
        }
    }
});
