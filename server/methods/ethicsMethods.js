Meteor.methods({
    updateEthics: function(mongoId, ethicsData){
        var fut = new future();

        ethics.schema.validate(ethicsData);

        if(mongoId){
            ethics.update({_id : mongoId} , {$set : ethicsData}, function(error,result){
                if (error){
                    fut.throw(error);
                } else if(result){
                    fut.return(true);
                }
            });
        } else{
            ethics.insert(ethicsData, function(error,result){
                if(error){
                    fut.throw(error);
                }else if(result){
                    fut.return(true);
                }
            });
        }

        try {
            return fut.wait();
        }
        catch(err) {
            throw new Meteor.Error(error);
        }
    },
    recacheEthicsHtml: function() {
        var journal = journalConfig.findOne();
        if (journal && journal.visitor) {
            Meteor.call('recacheUrl', journal.visitor + 'ethics');
        }
    }
});
