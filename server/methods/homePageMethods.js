Meteor.methods({
    updateHomePage: function(mongoId, data){
        var fut = new future();

        homePage.schema.validate(data);

        if(mongoId){
            homePage.update({_id : mongoId} , {$set : data}, function(error,result){
                if(error){
                    fut.throw(error);
                }else if(result){
                    fut.return(true);
                }
            });
        }else{
            homePage.insert(data, function(error,result){
                if(error){
                    fut.throw(error);
                }else if(result){
                    fut.return(true);
                }
            });
        }

        try {
            return fut.wait();
        }
        catch(err) {
            throw new Meteor.Error(error);
        }
    },
    recacheHomePageHtml: function() {
        var journal = journalConfig.findOne();
        if (journal && journal.visitor) {
            Meteor.call('recacheUrl', journal.visitor);
        }
    }
});
