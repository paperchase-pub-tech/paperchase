Meteor.methods({
    addArticleType: function(data){
        return articleTypes.insert(data);
    },
    updateArticleType: function(mongoId, data){
        // console.log('update', mongoId);
        var fut = new future();
        // name is required before submitting form. No need to check for existence.
        data.short_name = Meteor.general.makeShortName(data.name);

        if(!mongoId){
            // Add new article
            Meteor.call('addArticleType', data, function(error, newMongoId){
                if (error){
                    console.error('addArticle',error);
                    fut.throw(error);
                } else if(newMongoId){
                    fut.return(newMongoId);
                }
            });
        } else if(mongoId){
            // Update existing
            var updated = articleTypes.update({'_id' : mongoId}, {$set: data});
            if (updated) {
                fut.return(mongoId);
            }
        }

        try {
            return fut.wait();
        }
        catch(err) {
            throw new Meteor.Error(err);
        }
    }
});
