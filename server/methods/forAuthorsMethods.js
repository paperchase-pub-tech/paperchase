Meteor.methods({
    updateForAuthors: function(mongoId, data){
        var fut = new future();

        forAuthors.schema.validate(data);

        if(mongoId){
            forAuthors.update({_id : mongoId} , {$set : data}, function(error,result){
                if (error){
                    fut.throw(error);
                } else if(result){
                    fut.return(true);
                }
            });
        } else{
            forAuthors.insert(data, function(error,result){
                if(error){
                    fut.throw(error);
                }else if(result){
                    fut.return(true);
                }
            });
        }

        try {
            return fut.wait();
        }
        catch(err) {
            throw new Meteor.Error(error);
        }
    },
    recacheForAuthorsHtml: function() {
        var journal = journalConfig.findOne();
        if (journal && journal.visitor) {
            Meteor.call('recacheUrl', journal.visitor + 'for-authors');
        }
    }
});
