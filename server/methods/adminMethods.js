Meteor.methods({
    recacheUrl: function(url){
        var curl = Npm.require('curlrequest');
        var journal = journalConfig.findOne();
        if (journal && journal.site && journal.site.prerenderToken) {
            var options = {
                headers : 'Content-Type: application/json',
                url: 'http://api.prerender.io/recache',
                data: {
                    prerenderToken : journal.site.prerenderToken,
                    url: url
                }
            };
            curl.request(options, function (err, data) {
                if (err) {
                    console.error('Error: ', options.data.url, err);
                } else if (data){
                    // console.log(data, options.data.url);
                }
            });
        }
    }
});
