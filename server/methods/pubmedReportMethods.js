Meteor.methods({
    getPubMedReport: function(reportName) {
        return pubMedReport.find({reportName:reportName}).fetch();
    },
    clearPubMedReport: function(reportName) {
        return pubMedReport.remove({reportName:reportName});
    },
    createPubMedReport: function(opts) {
        this.unblock();

        var opts = opts || {};
        var piis = opts.piis || [];
        var reportName = opts.reportName || 'Unnamed report';
        var piiMeta = {};
        var timeout = 0;

        var config = journalConfig.findOne();
        var FETCH_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&retmode=xml&id=';
        var SEARCH_URL = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=json&term=' + config.journal.name + '+' + config.journal.issn;
        
        var asPromised = Npm.require('superagent-as-promised');
        var superagent = asPromised(Npm.require('superagent'));
        var parser = Npm.require('xml2json');

        var wrappedReport = Meteor.wrapAsync(createReport);
        return wrappedReport();

        function createReport(cb) {
            return initializeCollection()
            .then(createSearchIterable)
            .all()
            .then(createFetchIterable)
            .all()
            .then(massageData)
            .all()
            .then(function(res) {
                    console.log("All done");
                    cb(null);
                })
            .catch(function(err) {
                    console.log('Pubmed report creation failed', err);
                    cb(err);
                });

        }

        function initializeCollection() {
            return new Promise(function(resolve, reject) {
                    // Leaving this as placeholder. This had to happen on the client side, in the event, because we use the 'rawCollection' methods to duck the Meteor Fiber issue. Initializing the collection on the client lets us update the interface immediately, which does not happen when bypassing the Meteor wrappers for Mongo methods.
                    resolve(true);
                });
        }

        function createSearchIterable() {
            return new Promise(function(resolve, reject) {
                    var iterable = [];
                    piis.forEach(function(el,ind,arr) {
                            var pii = el;
                            iterable.push(new Promise(function(resolve, reject) {
                                        pubMedReport.rawCollection().update({reportName:reportName, "pii": pii}, {reportName:reportName,pii:pii, "fresh":false,status:"searching_for_pmid"}, {upsert:true}, function() {
                                                console.log("Searching for PMID: "+pii);
                                                var searchUrl = SEARCH_URL + '+'+config.journal.doi_prefix+'/'+config.journal.short_name+'.'+pii;
                                                console.log(searchUrl);
                                                timeout = timeout + 500;
                                                console.log("Search Timeout: "+timeout);
                                                setTimeout(function() {
                                                        resolve(superagent.get(searchUrl));
                                                    }, timeout); 
                                            });
                                    }));
                        });

                    resolve(iterable);
                });
        }

        function createFetchIterable(searchResponses) {
            return new Promise(function(resolve, reject) {
                    var iterable = [];
                    searchResponses.forEach(function(el,ind,arr) {
                            var data = el.body;
                            var pii = piis[ind];
                            var pmid = (data.esearchresult.errorlist !== undefined) ? 'NO RECORD' : data.esearchresult.idlist[0];
                            console.log("PII: "+pii);
                            console.log("PMID: "+pmid);
                            iterable.push(new Promise(function(resolve, reject) {
                                        pubMedReport.rawCollection().update({reportName:reportName, "pii": pii}, {$set: {status:"querying_metadata", "pmid":pmid}}, function() {
                                                console.log("Fetching Metadata: "+pii);
                                                if(pmid != "NO RECORD") {
                                                    var fetchUrl = FETCH_URL + pmid;
                                                    console.log(fetchUrl);
                                                    timeout = timeout + 500;
                                                    console.log("Fetch Timeout: "+timeout);
                                                    setTimeout(function() {
                                                            resolve(superagent.get(fetchUrl));
                                                        }, timeout); 
                                                }
                                                else {
                                                    resolve({pii: pii, 'no_record':true});
                                                }
                                            });
                                    }));
                        });

                    resolve(iterable);
                });
        }

        function massageData(fetchResponses) {
            console.log('MASSAGE PHASE REACHED');
            return new Promise(function(resolve, reject) {
                    var iterable = [];
                    fetchResponses.forEach(function(el,ind,arr) {
                            iterable.push(new Promise(function(resolve, reject) {
                                        if(el.no_record) {
                                            var pii = el.pii;
                                            var updateData = {
                                                fresh:true, 
                                                status:"data_ready",
                                                pii: pii,
                                                pmid: 'NO RECORD'
                                            };

                                            pubMedReport.rawCollection().update({reportName:reportName, "pii": pii}, {$set: updateData}, function() {
                                                    console.log("No PubMed data to massage: "+pii);
                                                    resolve(updateData);
                                                });

                                        }
                                        else {
                                            var data = JSON.parse(parser.toJson(el.text, {alternateTextNode: true}));

                                            var articleJson = data.PubmedArticleSet.PubmedArticle;
                                            var pii = 'N/A';
                                            var doi = 'N/A';
                                            var pubStatus = 'N/A';
                                            var ePubDate = 'N/A';
                                            var issuePubDate = 'N/A';

                                            // Title
                                            var articleTitle = (articleJson.MedlineCitation && articleJson.MedlineCitation.Article && articleJson.MedlineCitation.Article.ArticleTitle) ? articleJson.MedlineCitation.Article.ArticleTitle : 'N/A';

                                            // IDs
                                            if (articleJson.PubmedData && articleJson.PubmedData.ArticleIdList && articleJson.PubmedData.ArticleIdList.ArticleId) {
                                                articleJson.PubmedData.ArticleIdList.ArticleId.forEach(function(idData){
                                                        if (idData && idData.IdType && idData.IdType === 'pii') {
                                                            pii = idData._t;
                                                        } else if (idData && idData.IdType && idData.IdType === 'doi') {
                                                            doi = idData._t;
                                                        }
                                                    });
                                            }

                                            // Pub Status
                                            if (articleJson.PubmedData && articleJson.PubmedData.PublicationStatus) {
                                                pubStatus = articleJson.PubmedData.PublicationStatus;
                                            }

                                            var updateData = {
                                                fresh:true, 
                                                status:"data_ready",
                                                pii: pii,
                                                doi: doi,
                                                title: articleTitle,
                                                pubStatus: pubStatus  
                                            };
                                            pubMedReport.rawCollection().update({reportName:reportName, "pii": pii}, {$set: updateData}, function() {
                                                    console.log("Massaging data: "+pii);
                                                    resolve(updateData);
                                                });
                                        }
                                    }));
                        });

                    resolve(iterable);
                });
        }
    }
});
