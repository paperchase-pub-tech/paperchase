Meteor.articleTypes = {
    submitForm: function(e){
        // Collect article types form data, validate, send to method to update DB
        // validation will make sure that values are not empty. All inputs are required.
        e.preventDefault();
        Meteor.formActions.saving();
        var invalid = [];
        var forDb = {};
        var mongoId = e.target.getAttribute('data-id') ? e.target.getAttribute('data-id') : null;

        forDb.name = Meteor.clean.cleanString($('input[name="type-singular"]').val());
        forDb.plural = Meteor.clean.cleanString($('input[name="type-plural"]').val());
        forDb.nlm_type = Meteor.clean.cleanString($('#article-type-nlm_type').val());
        forDb.full_text = $('#article-type-full_text').prop('checked');
        forDb.abstract = $('#article-type-abstract').prop('checked');

        for(var key in forDb){
            if (key !=='full_text' && key !=='abstract' && !forDb[key] || forDb[key] === '') {
                invalid.push({
                    input_id : 'article-type-' + key,
                    message: key + ' is required.'
                });
            }
        }

        if (invalid.length > 0) {
            Meteor.formActions.invalid(invalid);
        } else {
            Meteor.call('updateArticleType', mongoId, forDb, function(error, result){
                if (error) {
                    Meteor.formActions.errorMessage('Could not save article type');
                } else if (result) {
                    Meteor.formActions.closeModal();
                    Router.go('ArticleTypeOverview', {_id :result});
                }
            });
        }

    }
};
