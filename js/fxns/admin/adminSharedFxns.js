Meteor.s3 = {
    pubmedXMLPath: function(s3){
        return s3.bucket + '/' + s3.folders.pubmed_xml;
    },
    upload: function(files, folder, cb){
        S3.upload({
            files: files,
            path: folder,
            unique_name: false
        },function(err,res){
            if (err) {
                console.error('S3 upload error',err);
                cb(err);
            } else if (res){
                cb(null, res);
            }
        });
    }
};
