Meteor.pageWithSections = {
    readyForm: function(){
        // Section title
        // ---------------
        $('.section-title').materialnote({
            onPaste: function(e){
                Meteor.formActions.removePastedStyle(e);
            },
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['undo', ['undo', 'redo', 'help']],
                ['misc', ['codeview']]
            ]
        });
        // Section content
        // ---------------
        $('.section-content').materialnote({
            onPaste: function(e){
                Meteor.formActions.removePastedStyle(e);
            },
            toolbar: [
                ['style', ['style', 'bold', 'italic', 'underline', 'strikethrough', 'clear']],
                ['undo', ['undo', 'redo', 'help']],
                ['misc', ['codeview','link']],
                ['para', ['ul', 'ol', 'paragraph', 'leftButton', 'centerButton', 'rightButton', 'justifyButton', 'outdentButton', 'indentButton']]
            ]
        });
    },
    formGetData: function () {
        var forDb = {};

        // Section title ---------------
        var title = $('.section-title').code();
        title = Meteor.clean.cleanWysiwyg(title);
        if(title !== ''){
            forDb.title = title;
        }

        // Section content ---------------
        var section = $('.section-content').code();
        section = Meteor.clean.cleanWysiwyg(section);
        if(section !== ''){
            forDb.content = section;
        }

        // Display ---------------
        forDb.display = $('#section-display').is(':checked');

        return forDb;
    },
    pageNameFromDataAttr: function(e){
        // get the page (about, ethics, homePage) from attribute
        return $(e.target).attr('data-id') ? $(e.target).attr('data-id').replace('page-section-', '') : null;
    },
    submitFormData: function (e) {
        e.preventDefault();
        var updateType = 'update',
            method,
            mongoId,
            forDb;

        page = Meteor.pageWithSections.pageNameFromDataAttr(e);

        if (page) {
            if (page === 'ethics') {
                method = 'updateEthics';
            } else if (page === 'about') {
                method = 'updateAbout';
            } else if (page === 'homePage') {
                method = 'updateHomePage';
            } else if (page === 'for_authors') {
                method = 'updateForAuthors';
            }

            if (method) {
                forDb = Meteor.pageWithSections.formGetData();

                // Check if section exists via Mongo ID hidden input
                mongoId = $('#section-mongo-id').val();

                if(!mongoId){
                    updateType = 'add';
                }

                Meteor.call(method, mongoId, forDb, function(error,result){
                    if(error){
                        console.error(method,error);
                        Meteor.formActions.errorMessage('Could not ' + updateType + ' ' + page + ' section.<br>' + error.reason);
                    } else if(result){
                        Meteor.pageWithSections.successPageSection();
                    }
                });
            } else {
                Meteor.formActions.errorMessage('Cannot determine method. Please contact IT.');
            }
        } else {
            Meteor.formActions.errorMessage('Cannot determine page. Please contact IT.');
        }

    },
    successPageSection: function(){
        // after success update/add section to: Ethics, About
        Meteor.formActions.successMessage();
        Session.set('showForm',false);
        Session.set('sectionId',null);
    },
};
